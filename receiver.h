#include <stdlib.h>

#define RXPIN_OUT		PB0
#define RXPORT_REG		PORTB
#define RXDDR_REG		DDRB

#define MIN_TE  (750) // 375us; 2MHz -> 0.5�s/tick; 10% error
#define MAX_TE  (916) // 458us
#define MIN_2TE (1500)// 750us
#define MAX_2TE (1832)// 916us
#define SHORT_PULSE(pw) (pw >= MIN_TE && pw <= MAX_TE)
#define LONG_PULSE(pw) (pw >= MIN_2TE && pw <= MAX_2TE)
#define RX_SIZE	(16)

enum receiverState_t {RX_IDLE, RX_START, RX_LOW, RX_LOW_SHORT, RX_HIGH, RX_HIGH_SHORT};
enum receiverState_t receiverState;

typedef union
{
	uint16_t data;
	struct {
		uint8_t command;
		uint8_t address;
	};
} forwardFrame_t;

typedef void (*rxForwardFrameReceivedCallback_t)(forwardFrame_t*);
typedef void (*rxErrorCallback_t)();
typedef void (*rxCallback_t)();

void initReceiver(rxForwardFrameReceivedCallback_t, rxErrorCallback_t, rxErrorCallback_t, rxCallback_t);
void startReceiver();
void stopReceiver();
