#include "random.h"

static uint16_t doADC();

void initADC()
{
	// disable PRADC
	PRR &= ~(1<<PRADC);


	// set prescaler and enable ADCSRA
	ADCSRA |= (1<<ADEN);
	ADCSRA |= ((1<<ADPS0) | (1<<ADPS2));

	// channel
	ADMUX &= ~(1<<MUX0);
	ADMUX &= ~(1<<MUX1);
	ADMUX &= ~(1<<MUX2);
	ADMUX &= ~(1<<MUX3);

	ADMUX |= ((1<<REFS0) | (1<<REFS1));
}

// naive implementation
uint16_t getRandomSeed()
{
	uint16_t seed = 0x0;
	for (int i=0; i<50; i++)
	{
		seed ^= doADC();
	}

	return seed;
}

static uint16_t doADC()
{
	uint16_t value = 0;
	ADCSRA |= (1<<ADSC);
	while (ADCSRA & (1<<ADSC));
	value = ADCW;
	return value;
}

