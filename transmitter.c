#include <avr/io.h>
#include <avr/interrupt.h>
#include "transmitter.h"
#include "debug.h"

txCallback_t txStartCallback;
txCallback_t txEndCallback;

static void initTxPin();
static void initCTCTimer();
static void startCTCTimer();
static void stopCTCTimer();
static void setTxStartCallback(txCallback_t f);
static void setTxEndCallback(txCallback_t f);

static 			uint8_t data;
static volatile uint8_t mask = 1<<(TX_SIZE-1);

ISR(TIMER2_COMPA_vect)
{
	switch(transmitterState)
	{
		case TX_IDLE:
			TXPORT_REG |= (1<<TXPIN_OUT);
			transmitterState = TX_START1;
			break;
		case TX_START1:
			TXPORT_REG &= ~(1<<TXPIN_OUT);
			transmitterState = TX_START2;
			break;
		case TX_START2:
			TXPORT_REG |= (1<<TXPIN_OUT);
			transmitterState = TX_BIT1;
			break;
		case TX_BIT1:
			if (data & mask)
				TXPORT_REG &= ~(1<<TXPIN_OUT);
			else
				TXPORT_REG |= (1<<TXPIN_OUT);

			transmitterState = TX_BIT2;
			break;
		case TX_BIT2:
			if (data & mask)
				TXPORT_REG |= (1<<TXPIN_OUT);
			else
				TXPORT_REG &= ~(1<<TXPIN_OUT);

			if (mask >>= 1)
				transmitterState = TX_BIT1;
			else
				transmitterState = TX_STOP1;

			break;
		case TX_STOP1:
			TXPORT_REG |= (1<<TXPIN_OUT);
			transmitterState = TX_STOP2;
			break;
		case TX_STOP2:
			TXPORT_REG |= (1<<TXPIN_OUT);
			transmitterState = TX_STOP3;
			break;
		case TX_STOP3:
			TXPORT_REG |= (1<<TXPIN_OUT);
			transmitterState = TX_END;
			break;
		case TX_END:
			TXPORT_REG |= (1<<TXPIN_OUT);
			transmitterState = TX_IDLE;
			stopTransmitter();
			txEndCallback();

			break;
		default:
			break;

	}
}

void transmit(uint8_t d)
{
	data = d;
	mask = 1<<(TX_SIZE-1);
	startTransmitter();
}

void initTransmitter(txCallback_t startCb, txCallback_t endCb)
{
	transmitterState = TX_IDLE;
	mask = 1<<(TX_SIZE-1);
	setTxStartCallback(startCb);
	setTxEndCallback(endCb);
	initTxPin();
	initCTCTimer();
}

void startTransmitter()
{
	transmitterState = TX_IDLE;
	//mask = 1<<(TX_SIZE-1);
	txStartCallback();
	startCTCTimer();
}

void stopTransmitter()
{
	stopCTCTimer();
}

static void initTxPin()
{
	TXDDR_REG |= (1<<TXPIN_OUT);
	TXPORT_REG |= (1<<TXPIN_OUT); // new
}

static void initCTCTimer()
{
	// CTC
	TCCR2A |= (1<<WGM21);

	// transmit op 2400 BAUD (clocktick per 416.66�s)
	OCR2A = 104;

	stopCTCTimer();
}

static void startCTCTimer()
{
	// prescaler /64
	TIMSK2 |= (1<<OCIE2A);
	TCCR2B &= ~(1<<CS20);
	TCCR2B &= ~(1<<CS21);
	TCCR2B |= (1<<CS22);
}

static void stopCTCTimer()
{
	TIMSK2 &= ~(1<<OCIE2A);
	TCCR2B &= ~(1<<CS20);
	TCCR2B &= ~(1<<CS21);
	TCCR2B &= ~(1<<CS22);
}

static void setTxStartCallback(txCallback_t f)
{
	txStartCallback = f;
}

static void setTxEndCallback(txCallback_t f)
{
	txEndCallback = f;
}
