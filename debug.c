#include <stdio.h>
#include "debug.h"

int printChar(char, FILE*);

FILE uart_str = FDEV_SETUP_STREAM(printChar, NULL, _FDEV_SETUP_RW);

int printChar(char character, FILE *stream)
{
	if(character == '\n') printChar('\r', stream);

	loop_until_bit_is_set(UCSR0A, UDRE0);

	UDR0 = character;

	return 0;
}

void initUART()
{

	UCSR0C |= (1<<UCSZ00);
	UCSR0C |= (1<<UCSZ01);
	//UCSR0C &= ~(1<<UCSZ02); //8bit
	UCSR0C |= (1<<UCSZ02);//9bit

	// set BAUD
	UBRR0H = (UBRR_value>>8);
	UBRR0L = UBRR_value;

	// enable transmission
	UCSR0B |= (1<<TXEN0);
}
