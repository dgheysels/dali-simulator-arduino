#include <avr/io.h>
#include <avr/interrupt.h>
#include "receiver.h"
#include "debug.h"

rxForwardFrameReceivedCallback_t forwardFrameReceivedCallback;
rxErrorCallback_t errorCallback;
rxErrorCallback_t timeoutCallback;
rxCallback_t startCallback;

static void resetReceiver();
static void initRxPin();
static void initCaptureTimer();
static void initTimeoutTimer();
static void resetCaptureTimer();
static void startCaptureTimer();
static void startTimeoutTimer();
static void stopCaptureTimer();
static void stopTimeoutTimer();
static void setForwardFrameReceivedCallback(rxForwardFrameReceivedCallback_t f);
static void setErrorCallback(rxErrorCallback_t f);
static void setTimeoutCallback(rxErrorCallback_t f);
static void setStartCallback(rxCallback_t f);

volatile uint8_t bit_value;
volatile int8_t  current_bit;
volatile uint16_t data;
volatile uint16_t pulse_width = 0;

// capture edges on pin PB0 (=ICP1)
ISR(TIMER1_CAPT_vect)
{
	// calculate capture time (TE of 2TE)
	pulse_width = ICR1;
	// switch between rising/falling flank
	TCCR1B ^= (1<<ICES1);
	TIFR1 |= 1<<ICF1;
	// reset counter
	TCNT1 = 0;

	switch(receiverState)
	{
	case RX_IDLE:
		receiverState = RX_START;
		startCallback();
		break;
	case RX_START:
		if (SHORT_PULSE(pulse_width))
		{
			receiverState = RX_HIGH;
			bit_value = 1;
		} else {
			errorCallback();
			printf("E: %u", pulse_width);
		}
		break;
	case RX_LOW:
		if (LONG_PULSE(pulse_width))
		{
			data |= (bit_value^1) << current_bit;
			current_bit--;
			bit_value = 1;
			receiverState = RX_HIGH;
		} else {
			receiverState = RX_LOW_SHORT;
		}
		break;
	case RX_LOW_SHORT:
		if (SHORT_PULSE(pulse_width))
		{
			data |= (bit_value) << current_bit;
			current_bit--;
			receiverState = RX_LOW;
		} else {
			errorCallback();
		}
		break;
	case RX_HIGH:
		if (LONG_PULSE(pulse_width))
		{
			data |= (bit_value^1) << current_bit;
			current_bit--;
			bit_value = 0;
			receiverState = RX_LOW;
		} else {
			receiverState = RX_HIGH_SHORT;
		}
		break;
	case RX_HIGH_SHORT:
		if (SHORT_PULSE(pulse_width))
		{
			data |= (bit_value) << current_bit;
			current_bit--;
			receiverState = RX_HIGH;
		} else {
			errorCallback();
		}
		break;
	default:
		break;
	}

	// alle bits ingelezen, start de timeouttimer
	// om de 2 stop bits te checken
	if (current_bit < 0)
	{
		stopCaptureTimer();
		startTimeoutTimer();
	}

}

ISR(TIMER0_COMPA_vect)
{
	// timeout occured
	// check if all bits are read
	stopTimeoutTimer();
	if (current_bit < 0)
	{
		forwardFrame_t frame;
		frame.data = data;
		forwardFrameReceivedCallback(&frame);
	} else {
		// timeout error
		timeoutCallback();
	}
}


void initReceiver(
		rxForwardFrameReceivedCallback_t forwardFrameReceivedCb,
		rxErrorCallback_t errorCb,
		rxErrorCallback_t timeoutCb,
		rxCallback_t startCb)
{
	setForwardFrameReceivedCallback(forwardFrameReceivedCb);
	setErrorCallback(errorCb);
	setTimeoutCallback(timeoutCb);
	setStartCallback(startCb);
	initRxPin();
	initCaptureTimer();
	initTimeoutTimer();
}

void startReceiver()
{
	resetReceiver();
	startCaptureTimer();
}

void stopReceiver()
{
	stopCaptureTimer();
	stopTimeoutTimer();
}

static void resetReceiver()
{
	receiverState = RX_IDLE;
	current_bit = RX_SIZE-1; // MSB eerst receiven
	data = 0;
}

static void initRxPin()
{
	RXDDR_REG &= ~(1<<RXPIN_OUT);
	RXPORT_REG |= (1<<RXPIN_OUT);
}

static void initCaptureTimer()
{
	// datasheet P. 118

	// standard mode
	TCCR1A &= ~(1<<WGM10);
	TCCR1A &= ~(1<<WGM11);
	TCCR1B &= ~(1<<WGM12);
	TCCR1B &= ~(1<<WGM13);

	//  input capture noise canceler
	TCCR1B |= (1<<ICNC1);
}

static void initTimeoutTimer()
{
	// clear timer on compare match (CTC)
	// Clear OC0A on Compare Match
	TCCR0A &= ~(1<<WGM00);
	TCCR0A |= (1<<WGM01);
	TCCR0B &= ~(1<<WGM02);
	TCCR0A &= ~(1<<COM0A0);
	TCCR0A &= ~(1<<COM0A1);

	// /256 prescaler

	// OCR0A = timer resolution
	// TOP = 115
	//  = 2*MAX_2TE = 1832 �s
	//  = prescaler 256 = 16�s / tick
	//  = 1832 /16 = 114 ticks
	OCR0A = 114;
}

static void resetCaptureTimer()
{
	TCNT1 = 0;
	// start capturing on falling edge
	TCCR1B &= ~(1<<ICES1);
	TIFR1 |= 1<<ICF1;
}

static void startCaptureTimer()
{
	resetCaptureTimer();

	// prescaler /8
	TCCR1B &= ~(1<<CS10);
	TCCR1B |= (1<<CS11);
	TCCR1B &= ~(1<<CS12);
	TIMSK1 |= (1<<ICIE1);
}

static void startTimeoutTimer()
{
	TCCR0B &= ~(1<<CS00);
	TCCR0B &= ~(1<<CS01);
	TCCR0B |= (1<<CS02); // 256 prescaler
	TIMSK0 |= (1<<OCIE0A);
}

static void stopCaptureTimer()
{
	TIMSK1 &= ~(1<<ICIE1);
	TCCR1B &= ~(1<<CS10);
	TCCR1B &= ~(1<<CS11);
	TCCR1B &= ~(1<<CS12);
}

static void stopTimeoutTimer()
{
	TIMSK0 &= ~(1<<OCIE0A);
	TCCR0B &= ~(1<<CS00);
	TCCR0B &= ~(1<<CS01);
	TCCR0B &= ~(1<<CS02);
}

static void setForwardFrameReceivedCallback(rxForwardFrameReceivedCallback_t f)
{
	forwardFrameReceivedCallback = f;
}

static void setErrorCallback(rxErrorCallback_t f)
{
	errorCallback = f;
}

static void setTimeoutCallback(rxErrorCallback_t f)
{
	timeoutCallback = f;
}

static void setStartCallback(rxCallback_t f)
{
	startCallback = f;
}
