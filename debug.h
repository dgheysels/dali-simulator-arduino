#include <avr/io.h>
#include <stdio.h>

#define F_OSC		16000000
#define BAUD_RATE	57600
#define UBRR_value	((F_OSC / (BAUD_RATE * 16UL)) -1)

FILE uart_str;

void initUART();
