#include <stdint.h>

#define NUMBER_SLAVES	2

typedef struct
{
	uint8_t	long_address_h;
	uint8_t	long_address_m;
	uint8_t	long_address_l;
} long_address_t;

typedef struct
{
	long_address_t 	long_address;
	uint8_t			short_address;
	long_address_t	search_buffer;
	uint8_t			withdrawn;

	uint8_t			arc_power_level;
} slave_t;

typedef uint8_t (*commandHandler_t)(slave_t*);

extern slave_t* slaves[];

extern commandHandler_t commands[161];

void create_slaves();
void destroy_slaves();
slave_t* find_slave(uint8_t short_address);

uint8_t set_directPowerLevel(slave_t* slave, uint8_t power);

void generate_random_address(slave_t* slave);
uint8_t compare(slave_t* slave);
void withdraw(slave_t* slave);
void set_searchbuffer_h(slave_t* slave, uint8_t byte);
void set_searchbuffer_m(slave_t* slave, uint8_t byte);
void set_searchbuffer_l(slave_t* slave, uint8_t byte);
void program_shortaddress(slave_t* slave, uint8_t shortaddress);
