################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../debug.c \
../main.c \
../receiver.c \
../transmitter.c 

OBJS += \
./debug.o \
./main.o \
./receiver.o \
./transmitter.o 

C_DEPS += \
./debug.d \
./main.d \
./receiver.d \
./transmitter.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -I"C:\WinAVR\avr\include" -I"C:\WinAVR\lib\gcc\avr\4.3.3\include" -I"C:\WinAVR\lib\gcc\avr\4.3.3\include-fixed" -Wall -g2 -gstabs -O0 -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


