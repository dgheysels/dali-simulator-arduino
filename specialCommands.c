#include <stdlib.h>
#include <stdint.h>
#include "specialCommands.h"
#include "slaves.h"

void handle_randomize(uint8_t address, uint8_t data)
{
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		generate_random_address(slaves[i]);
	}
}

uint8_t handle_compare(uint8_t address, uint8_t data)
{
	uint8_t result = 0x00;
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		if (!(slaves[i]->withdrawn))
		{
			result |= compare(slaves[i]);
		}
	}

	return result;
}

void handle_withdraw(uint8_t address, uint8_t data)
{
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		withdraw(slaves[i]);
	}
}

void handle_search_addr_h(uint8_t address, uint8_t data)
{
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		if (!slaves[i]->withdrawn)
		{
			set_searchbuffer_h(slaves[i], data);
		}
	}
}

void handle_search_addr_m(uint8_t address, uint8_t data)
{
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		if (!slaves[i]->withdrawn)
		{
			set_searchbuffer_m(slaves[i], data);
		}
	}
}

void handle_search_addr_l(uint8_t address, uint8_t data)
{
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		if (!slaves[i]->withdrawn)
		{
			set_searchbuffer_l(slaves[i], data);
		}
	}
}

void handle_program_shortaddress(uint8_t address, uint8_t data)
{
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		if (!slaves[i]->withdrawn)
		{
			program_shortaddress(slaves[i], (data >> 1) & 0x3F);
		}
	}
}
