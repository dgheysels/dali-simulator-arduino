#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "slaves.h"

static uint8_t handle_extinguish(slave_t* slave);
static uint8_t handle_dimUp(slave_t* slave);
static uint8_t handle_dimDown(slave_t* slave);
static uint8_t handle_stepUpNoIgnite(slave_t* slave);
static uint8_t handle_stepDownNoSwitchOff(slave_t* slave);
static uint8_t handle_setMaxIgnite(slave_t* slave);
static uint8_t handle_setMinIgnite(slave_t* slave);
static uint8_t handle_stepDownSwitchOff(slave_t* slave);
static uint8_t handle_stepUpIgnite(slave_t* slave);
static uint8_t handle_print(slave_t* slave);
static uint8_t handle_query_actual_level(slave_t* slave);

static uint8_t handle_NI(slave_t* slave);

slave_t* slaves[NUMBER_SLAVES];
commandHandler_t commands[161] =
{
/*0x00*/handle_extinguish,
		handle_dimUp,
		handle_dimDown,
		handle_stepUpNoIgnite,
		handle_stepDownNoSwitchOff,
		handle_setMaxIgnite,
		handle_setMinIgnite,
		handle_stepDownSwitchOff,
		handle_stepUpIgnite,

	/* reserved */
/*0x09*/handle_print,
		handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

	/* goto scene */
/*0x10*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,
/*0x18*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

/*0x20*/handle_NI,
/*0x21*/handle_NI,

	/* reserved */
/*0x22*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

/*0x2A*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

	/* reserved */
/*0x30*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,
/*0x38*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

	/* store DTR as scene */
/*0x40*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,
/*0x48*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

	/* remove from scene */
/*0x50*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,
/*0x58*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

	/* add to group */
/*0x60*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,
/*0x68*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

	/* remove from group */
/*0x70*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,
/*0x78*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

	/* store DTR as short address */
/*0x80*/handle_NI,

	/* reserved*/
/*0x81*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,
/*0x89*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

/*0x90*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,
/*0x98*/handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI, handle_NI,

/*0xA0*/handle_query_actual_level
};

void create_slaves()
{
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		slaves[i] = malloc(sizeof(slave_t));
		slaves[i]->short_address = 0x00;
		slaves[i]->long_address = (long_address_t){0x00, 0x00, 0x00};
		slaves[i]->search_buffer = (long_address_t){0x00, 0x00, 0x00};
		slaves[i]->withdrawn = 0x00;
		slaves[i]->arc_power_level = 0x00;
	}
}

void destroy_slaves()
{
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		free(slaves[i]);
	}
}

slave_t* find_slave(uint8_t short_address)
{
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		if (slaves[i]->short_address == short_address)
		{
			return slaves[i];
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////////////
/// COMMANDS
////////////////////////////////////////////////////////////////////////////////////////
uint8_t set_directPowerLevel(slave_t* slave, uint8_t power)
{
	slave->arc_power_level = power;
	return 0x00;
}

static uint8_t handle_extinguish(slave_t* slave)
{
	slave->arc_power_level = 0x00;
	return 0x00;
}

static uint8_t handle_dimUp(slave_t* slave)
{
	return 0x00;
}

static uint8_t handle_dimDown(slave_t* slave)
{
	return 0x00;
}

static uint8_t handle_stepUpNoIgnite(slave_t* slave)
{
	if (slave->arc_power_level != 0x00 && slave->arc_power_level < 0xFF)
	{
		slave->arc_power_level++;
	}
	return 0x00;
}

static uint8_t handle_stepDownNoSwitchOff(slave_t* slave)
{
	if (slave->arc_power_level > 0x01)
	{
		slave->arc_power_level--;
	}
	return 0x00;
}

static uint8_t handle_setMaxIgnite(slave_t* slave)
{
	slave->arc_power_level = 0xFF;
	return 0x00;
}

static uint8_t handle_setMinIgnite(slave_t* slave)
{
	slave->arc_power_level = 0x01;
	return 0x00;
}

static uint8_t handle_stepDownSwitchOff(slave_t* slave)
{
	if (slave->arc_power_level > 0x00)
	{
		slave->arc_power_level--;
	}
	return 0x00;
}

static uint8_t handle_stepUpIgnite(slave_t* slave)
{
	if (slave->arc_power_level < 0xFF)
	{
		slave->arc_power_level++;
	}
	return 0x00;
}

static uint8_t handle_print(slave_t* slave)
{
	printf("la:{%x %x %x}-sa:{%x}-b:{%x %x %x}-w:{%x} >> %hhu\n\r",
			slave->long_address.long_address_h, slave->long_address.long_address_m, slave->long_address.long_address_l,
			slave->short_address,
			slave->search_buffer.long_address_h, slave->search_buffer.long_address_m, slave->search_buffer.long_address_l,
			slave->withdrawn,
			slave->arc_power_level);
	return 0x00;
}

static uint8_t handle_NI(slave_t* slave)
{
	printf("command not implemented\n\r");
	return 0x00;
}

static uint8_t handle_query_actual_level(slave_t* slave)
{
	return slave->arc_power_level;
}

////////////////////////////////////////////////////////////////////////////////////////
/// SPECIAL COMMANDS
////////////////////////////////////////////////////////////////////////////////////////
void generate_random_address(slave_t* slave)
{
	int r = rand();
	uint8_t h = (r & 0xFF00) >> 8;
	uint8_t m = (r & 0x0FF0) >> 4;
	uint8_t l = r & 0x00FF;

	slave->long_address.long_address_h = h;
	slave->long_address.long_address_m = m;
	slave->long_address.long_address_l = l;
}

uint8_t compare(slave_t* slave)
{
	if (slave->long_address.long_address_h < slave->search_buffer.long_address_h)
	{
		return 0xFF;
	}
	else if (slave->long_address.long_address_h > slave->search_buffer.long_address_h)
	{
		return 0x00;
	}
	else
	{
		if (slave->long_address.long_address_m < slave->search_buffer.long_address_m)
		{
			return 0xFF;
		}
		else if (slave->long_address.long_address_m > slave->search_buffer.long_address_m)
		{
			return 0x00;
		}
		else
		{
			if (slave->long_address.long_address_l <= slave->search_buffer.long_address_l)
			{
				return 0xFF;
			}
			else
			{
				return 0x00;
			}
		}
	}
}

void withdraw(slave_t* slave)
{
	if ((slave->long_address.long_address_h == slave->search_buffer.long_address_h) &&
		(slave->long_address.long_address_m == slave->search_buffer.long_address_m) &&
		(slave->long_address.long_address_l == slave->search_buffer.long_address_l))
	{
		slave->withdrawn = 1;
	}
}
void set_searchbuffer_h(slave_t* slave, uint8_t byte)
{
	slave->search_buffer.long_address_h = (byte);
}

void set_searchbuffer_m(slave_t* slave, uint8_t byte)
{
	slave->search_buffer.long_address_m = (byte);
}

void set_searchbuffer_l(slave_t* slave, uint8_t byte)
{
	slave->search_buffer.long_address_l = (byte);
}

void program_shortaddress(slave_t* slave, uint8_t shortaddress)
{
	if ((slave->long_address.long_address_h == slave->search_buffer.long_address_h) &&
		(slave->long_address.long_address_m == slave->search_buffer.long_address_m) &&
		(slave->long_address.long_address_l == slave->search_buffer.long_address_l))
	{
		slave->short_address = shortaddress;

	}
}
