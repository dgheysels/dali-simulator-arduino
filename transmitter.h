#include <stdlib.h>

#define TXPIN_OUT		PD7
#define TXPORT_REG		PORTD
#define TXDDR_REG		DDRD

#define TX_SIZE (8)

enum transmitterState_t {TX_IDLE, TX_START1, TX_START2, TX_BIT1, TX_BIT2, TX_STOP1, TX_STOP2, TX_STOP3, TX_END};
enum transmitterState_t transmitterState;

typedef void (*txCallback_t)();

void transmit(uint8_t data);
void initTransmitter(txCallback_t, txCallback_t);
void startTransmitter();
void stopTransmitter();
