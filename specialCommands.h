#include <stdint.h>

void handle_randomize(uint8_t address, uint8_t data);
uint8_t handle_compare(uint8_t address, uint8_t data);
void handle_withdraw(uint8_t address, uint8_t data);
void handle_search_addr_h(uint8_t address, uint8_t data);
void handle_search_addr_m(uint8_t address, uint8_t data);
void handle_search_addr_l(uint8_t address, uint8_t data);
void handle_program_shortaddress(uint8_t address, uint8_t data);
