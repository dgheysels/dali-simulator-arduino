#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include "specialCommands.h"
#include "slaves.h"
#include "receiver.h"
#include "transmitter.h"
#include "debug.h"
#include "random.h"

enum dalistate_t {DALI_IDLE, DALI_TRANSMITTING, DALI_RECEIVING, DALI_TIMEOUT, DALI_ERROR};
volatile enum dalistate_t dali_state;

void forwardFrameReceived(forwardFrame_t* data);
void error();
void timeout();
void startReceiving();
void startTransmitting();
void endTransmitting();

void handleBroadcastCommand(forwardFrame_t*);
void handleCommand(forwardFrame_t*);
void handleSpecialCommand(forwardFrame_t*);

volatile forwardFrame_t* received_frame;

int main()
{
	stdout = &uart_str;
	initUART();
	initADC();

	srand(getRandomSeed());

	create_slaves();

	dali_state = DALI_IDLE;
	received_frame = NULL;

	initTransmitter(startTransmitting, endTransmitting);
	initReceiver(forwardFrameReceived, error, timeout, startReceiving);
	startReceiver();

	printf("receiver started...\n");

	sei();

	while (1)
	{

		if (received_frame != NULL)
		{
			//printf("received_frame: 0x%x\n\r", received_frame->data);
			if ((received_frame->address & 0xFE) == 0xFE)
			{
				handleBroadcastCommand(received_frame);
			}
			else if ((received_frame->address & 0xA1) == 0xA1 ||
					 (received_frame->address & 0xC1) == 0xC1)
			{
				handleSpecialCommand(received_frame);
			}
			else
			{
				handleCommand(received_frame);
			}


			while (dali_state != DALI_IDLE);

			free(received_frame);
			received_frame = NULL;

			initTransmitter(startTransmitting, endTransmitting);
			startReceiver();
		}

		switch (dali_state)
		{
		case DALI_ERROR:
			printf("\nERROR");
			free(received_frame);
			received_frame = NULL;
			initTransmitter(startTransmitting, endTransmitting);
			dali_state = DALI_IDLE;
			startReceiver();
			break;
		default: break;

		}
	}

	return 0;
}

void handleBroadcastCommand(forwardFrame_t* frame)
{
	uint8_t result = 0x00;
	for (int i=0; i<NUMBER_SLAVES; i++)
	{
		if (!(frame->address & 0x01))
		{
			// direct power
			result = set_directPowerLevel(slaves[i], frame->data);
		}
		else
		{
			result |= commands[frame->command](slaves[i]);
		}
	}
	if (result)
	{
		_delay_us(3000);
		transmit(result);
	}
}

void handleSpecialCommand(forwardFrame_t* frame)
{
	uint8_t result = 0x00;
	switch(frame->address)
	{
	case 0xA1:
		break;
	case 0xA5:
		break;
	case 0xA7:
		handle_randomize(frame->address, frame->command);
		break;
	case 0xA9:
		result = handle_compare(frame->address, frame->command);
		_delay_us(3000);
		transmit(result);
		break;
	case 0xAB:
		handle_withdraw(frame->address, frame->command);
		break;
	case 0xB1:
		handle_search_addr_h(frame->address, frame->command);
		break;
	case 0xB3:
		handle_search_addr_m(frame->address, frame->command);
		break;
	case 0xB5:
		handle_search_addr_l(frame->address, frame->command);
		break;
	case 0xB7:
		handle_program_shortaddress(frame->address, frame->command);
		break;
	default: break;
	}
}

void handleCommand(forwardFrame_t* frame)
{
	if ((frame->address & 0x80) == 0x80)
	{
		// group address
		uint8_t group_address = ((frame->address) >> 1) & 0x0F;
	}
	else
	{
		uint8_t single_address = ((frame->address) >> 1) & 0x3F;
		slave_t* slave = find_slave(single_address);
		if (slave != NULL)
		{
			uint8_t result = 0x00;
			if (!(frame->address & 0x01))
			{
				// direct power
				result = set_directPowerLevel(slave, frame->data);
			}
			else
			{
				result = commands[frame->command](slave);
				if (result)
				{
					_delay_us(3000);
					transmit(result);
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////
/// CALLBACKS
////////////////////////////////////////////////////////////////////////////////////////
void forwardFrameReceived(forwardFrame_t* frame)
{
	received_frame = malloc(sizeof(forwardFrame_t));
	received_frame->address = frame->address;
	received_frame->command = frame->command;

	dali_state = DALI_IDLE;
}

void error()
{
	stopReceiver();
	dali_state = DALI_ERROR;
}

void timeout()
{
	dali_state = DALI_TIMEOUT;
}

void startReceiving()
{
	dali_state = DALI_RECEIVING;
}

void startTransmitting()
{
	dali_state = DALI_TRANSMITTING;
}

void endTransmitting()
{
	dali_state = DALI_IDLE;
}



